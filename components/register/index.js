import React, { useState, useEffect } from 'react';
import classes from './register.module.scss';
import { Consumer } from 'contexts';
import { isEmpty } from 'underscore';

const Register = props => {
  const { user, registerBox, registerUser, setToken, setShowRegister } = props;
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [showError, setShowError] = useState('');
  const [showSuccess, setShowSuccess] = useState('');
  const [loading, setLoading] = useState(false);

  const handleRegister = e => {
    setShowError('');
    e.preventDefault();
    if (password !== confirmPassword) {
      setShowError('passwords do not match');
      return;
    }
    setLoading(true);
    registerUser({ username, password }).then(token => {
      if (!token) {
        setShowError('Use a different username');
        setLoading(false);
        return;
      }
      setToken(token);
      setLoading(false);
    });
  };

  const handleClose = () => {
    setShowRegister(false);
  };

  useEffect(() => {
    if (!isEmpty(user)) {
      setShowSuccess('successfully registered');
    }
  }, [user]);

  return (
    <div className={classes.container}>
      <form onSubmit={handleRegister} ref={registerBox} className={classes.box}>
        <label>
          username
          <input
            disabled={loading}
            type="input"
            value={username}
            onChange={e => setUsername(e.currentTarget.value)}
          />
        </label>
        <label>
          password
          <input
            disabled={loading}
            type="password"
            value={password}
            onChange={e => setPassword(e.currentTarget.value)}
          />
        </label>
        <label>
          confirm password
          <input
            disabled={loading}
            type="password"
            value={confirmPassword}
            onChange={e => setConfirmPassword(e.currentTarget.value)}
          />
        </label>
        {showError && <p className={classes.error}>{showError}</p>}
        {showSuccess && <p className={classes.success}>{showSuccess}</p>}
        {showSuccess ? (
          <button onClick={handleClose}>close this box</button>
        ) : (
          <button disabled={loading} type="submit">
            register
          </button>
        )}
      </form>
    </div>
  );
};

export default Consumer(
  React.memo(Register),
  ({ user, registerUser, setToken }) => ({
    user,
    registerUser,
    setToken,
  })
);
