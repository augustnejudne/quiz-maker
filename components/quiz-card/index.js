import React, { useState, useRef } from 'react';
import moment from 'moment';
import classes from './quiz-card.module.scss';
import icons from 'assets/img/icons/categories/categories';
import Link from 'next/link';
import { useClickOutside } from 'hooks';
import { useRouter } from 'next/router';

const QuizCard = props => {
  const router = useRouter();
  const {
    deleteQuiz,
    update,
    displayOnly,
    quiz: {
      _id,
      createdAt,
      creator: { _id: creatorId, username },
      title,
      questions,
      takers,
      icon,
      colorScheme,
    },
  } = props;
  const confirmDeleteBox = useRef();
  const [showConfirmDelete, setShowConfirmDelete] = useState(false);

  useClickOutside(confirmDeleteBox, setShowConfirmDelete);

  const handleDeleteQuiz = async () => {
    const deleted = await deleteQuiz(_id);
    if (deleted) {
      setShowConfirmDelete(false);
    }
  };

  const handleEditQuiz = () => {
    router.push(`/edit/${_id}`);
  };

  const renderButton = () => {
    if (displayOnly) {
      return <button className={classes.displayButton}>Take Quiz</button>;
    }

    return (
      <Link href={`/quizzes/${_id}`}>
        <a>
          <button>Take Quiz</button>
        </a>
      </Link>
    );
  };

  const setBackground = colorScheme => {
    switch (colorScheme) {
      case 0:
        return 'linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%)';
      case 1:
        return 'linear-gradient(120deg, #f6d365 0%, #fda085 100%)';
      case 2:
        return 'linear-gradient(120deg, #d4fc79 0%, #96e6a1 100%)';
      case 3:
        return 'linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%)';
      case 4:
        return 'linear-gradient(120deg, #a1c4fd 0%, #c2e9fb 100%)';
      case 5:
        return 'linear-gradient(120deg, #e0c3fc 0%, #8ec5fc 100%)';
      case 6:
        return 'linear-gradient(45deg, #ff9a9e 0%, #fad0c4 100%)';
      default:
        return 'linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%)';
    }
  };

  const background = {
    background: setBackground(colorScheme),
  };

  return (
    <div className={classes.quizContainer} style={background}>
      {showConfirmDelete && (
        <div className={classes.confirmDeleteContainer}>
          <div
            ref={confirmDeleteBox}
            className={classes.confirmDeleteButtonsBox}
          >
            <button onClick={handleDeleteQuiz}>Yes</button>
            <button onClick={() => setShowConfirmDelete(false)}>No</button>
          </div>
        </div>
      )}
      {update && (
        <div className={classes.updateActions}>
          <button onClick={handleEditQuiz} className={classes.editButton}>
            edit
          </button>
          <button
            onClick={() => setShowConfirmDelete(true)}
            className={classes.deleteButton}
          >
            delete
          </button>
        </div>
      )}
      <div className={classes.creatorContainer}>
        <div className={classes.nameDateContainer}>
          <h6>
            {displayOnly ? (
              username
            ) : (
              <Link href={`/users/${creatorId}`}>
                <a>{username}</a>
              </Link>
            )}
          </h6>
          <p>{moment(createdAt).fromNow()}</p>
        </div>
      </div>
      <div className={classes.quizDetailsContainer}>
        <div className={classes.iconContainer}>
          <img src={icons[icon]} alt="" />
        </div>
        <div className={classes.detailsTextContainer}>
          <h6>{title}</h6>
          <p>
            {questions.length} question{questions.length > 1 ? 's' : ''}
          </p>
          <p>{takers} takers</p>
        </div>
      </div>
      <div className={classes.buttonContainer}>{renderButton()}</div>
    </div>
  );
};

export default React.memo(QuizCard);
