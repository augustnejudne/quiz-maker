import React, { useState, useEffect } from 'react';
import classes from './question-card.module.scss';

const Answer = props => {
  const {
    i,
    answer,
    correctAnswer,
    correctAnswers,
    setCorrectAnswers,
    questionsAnswered,
    setQuestionsAnswered,
    answered,
    setAnswered,
  } = props;
  const [selected, setSelected] = useState(false);

  const handleClick = () => {
    if (answered) return;
    if (i === correctAnswer) {
      setCorrectAnswers(correctAnswers + 1);
    }
    setSelected(true);
    setAnswered(true);
    setQuestionsAnswered(questionsAnswered + 1);
  };

  const setClassName = () => {
    if ((selected || answered) && i === correctAnswer) {
      return classes.correct;
    }

    if (selected && i !== correctAnswer) {
      return classes.wrong;
    }

    if (!answered) {
      return classes.option;
    }

    return;
  };

  return (
    <p onClick={handleClick} className={setClassName()}>
      {answer}
    </p>
  );
};

const QuestionCard = props => {
  const {
    i,
    question: { question, answers, correctAnswer },
    correctAnswers,
    setCorrectAnswers,
    questionsAnswered,
    setQuestionsAnswered,
  } = props;
  const [answered, setAnswered] = useState(false);
  return (
    <div className={classes.questionCardContainer}>
      <div className={classes.top}>
        <h3>{i + 1}</h3>
        <h5>{question}</h5>
      </div>
      <div className={classes.bottom}>
        {answers.map((answer, i) => {
          return (
            <Answer
              answered={answered}
              setAnswered={setAnswered}
              answer={answer}
              correctAnswer={correctAnswer}
              correctAnswers={correctAnswers}
              setCorrectAnswers={setCorrectAnswers}
              questionsAnswered={questionsAnswered}
              setQuestionsAnswered={setQuestionsAnswered}
              i={i}
              key={i}
            />
          );
        })}
      </div>
    </div>
  );
};

export default React.memo(QuestionCard);
