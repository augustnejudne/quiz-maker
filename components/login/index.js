import React, { useState, useEffect } from 'react';
import classes from './login.module.scss';
import { Consumer } from 'contexts';
import { isEmpty } from 'underscore';

const FBLogin = props => {
  const { setShowLogin, loginBox, loginUser, setToken, user } = props;
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [showError, setShowError] = useState('');
  const [showSuccess, setShowSuccess] = useState('');
  const [loading, setLoading] = useState(false);

  const handleLogin = e => {
    e.preventDefault();
    setShowError('');
    setLoading(true);
    loginUser({ username, password }).then(token => {
      if (!token) {
        setShowError('username or password is wrong');
        setLoading(false);
        return;
      }
      setToken(token);
      setLoading(false);
    });
  };

  const handleClose = () => {
    setShowLogin(false);
  };

  useEffect(() => {
    if (!isEmpty(user)) {
      setShowSuccess('successfully logged in');
    }
  }, [user]);

  return (
    <div className={classes.container}>
      <form onSubmit={handleLogin} ref={loginBox} className={classes.box}>
        <label>
          username
          <input
            disabled={loading}
            type="input"
            value={username}
            onChange={e => setUsername(e.currentTarget.value)}
          />
        </label>
        <label>
          password
          <input
            disabled={loading}
            type="password"
            value={password}
            onChange={e => setPassword(e.currentTarget.value)}
          />
        </label>
        {showError && <p className={classes.error}>{showError}</p>}
        {showSuccess && <p className={classes.success}>{showSuccess}</p>}
        {showSuccess ? (
          <button onClick={handleClose}>close this box</button>
        ) : (
          <button disabled={loading} type="submit">
            login
          </button>
        )}
      </form>
    </div>
  );
};

export default Consumer(
  React.memo(FBLogin),
  ({ loginUser, setToken, user }) => ({
    loginUser,
    setToken,
    user,
  })
);
