import React, { Fragment, useState, useRef } from 'react';
import { useRouter } from 'next/router';
import classes from './nav.module.scss';
import { Consumer } from 'contexts';
import { isEmpty } from 'underscore';
import { useClickOutside } from 'hooks';

const Nav = props => {
  const { setShowLogin, setShowRegister, user } = props;
  const router = useRouter();
  const [showMenu, setShowMenu] = useState(false);
  const menu = useRef(null);

  useClickOutside(menu, setShowMenu);

  const handleIconClick = () => {
    setShowMenu(!showMenu);
  };

  const handleNav = route => {
    if (router.route === route) return;
    if (isEmpty(user)) {
      if (route === '/login') {
        setShowLogin(true);
      }
      if (route === '/register') {
        setShowRegister(true);
      }
      return;
    }
    router.push(route);
  };

  const routes = [
    {
      display: '#register',
      route: '/register',
    },
    {
      display: '#login',
      route: '/login',
    },
    {
      display: '#create',
      route: '/create',
    },
    {
      display: '#explore',
      route: '/',
    },
    {
      display: '#mine',
      route: '/mine',
    },
    {
      display: '#logout',
      route: '/logout',
    },
  ];

  const renderRoutes = () => {
    const authRoutes = ['/logout', '/create', '/mine'];
    return routes.map((r, i) => {
      const { display, route } = r;
      if (isEmpty(user) && authRoutes.includes(route)) return;
      if (!isEmpty(user) && route === '/login') return;
      if (!isEmpty(user) && route === '/register') return;
      return (
        <h2
          style={{
            color: router.route === route ? '#03a9f4' : 'white',
          }}
          onClick={() => handleNav(route)}
          key={i}
        >
          {display}
        </h2>
      );
    });
  };

  return (
    <Fragment>
      <div onClick={handleIconClick} className={classes.iconContainer}>
        <img
          className={classes.image}
          src={!isEmpty(user) ? user.picture : ''}
          alt={!isEmpty(user) ? user.displayName : ''}
        />
        <div className={classes.bar} />
        <div className={classes.bar} />
        <div className={classes.bar} />
      </div>
      {showMenu && (
        <div ref={menu} className={classes.menuContainer}>
          <div className={classes.menu}>{renderRoutes()}</div>
        </div>
      )}
    </Fragment>
  );
};

export default Consumer(React.memo(Nav), ({ user }) => ({
  user,
}));
