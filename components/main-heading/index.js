import React from 'react';
import classes from './heading.module.scss';
import Link from 'next/link';

const MainHeading = () => {
  return (
    <div className={classes.headerContainer}>
      <Link href="/">
        <a>
          <h1 className={classes.mainTitle}>Quiz Maker</h1>
          <p className={classes.subTitle}>
            Create quizzes and share them with your friends.
          </p>
        </a>
      </Link>
    </div>
  );
};

export default React.memo(MainHeading);
