import React from 'react';
import classes from './create-question-card.module.scss';

const CreateQuestionCard = props => {
  const {
    questions,
    questionIndex,
    setQuestions,
    question,
    handleRemoveQuestion,
  } = props;
  const letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];

  const correctAnswer = question.correctAnswer;

  const addOption = () => {
    const questionsCopy = questions;
    if (questionsCopy[questionIndex].answers.length === 10) return;
    questionsCopy[questionIndex].answers.push('');
    setQuestions([...questionsCopy]);
  };

  const removeOption = index => {
    const questionsCopy = questions;
    if (questionsCopy[questionIndex].answers.length === 2) return;
    questionsCopy[questionIndex].answers.splice(index, 1);
    setQuestions([...questionsCopy]);
  };

  const handleOptionChange = (i, e) => {
    const questionsCopy = questions;
    questionsCopy[questionIndex].answers[i] = e.currentTarget.value;
    setQuestions([...questionsCopy]);
  };

  const handleQuestionChange = e => {
    e.currentTarget.style.height = e.currentTarget.scrollHeight + 'px';
    const questionsCopy = questions;
    questionsCopy[questionIndex].question = e.currentTarget.value;
    setQuestions([...questionsCopy]);
  };

  const setCorrectAnswer = i => {
    const questionsCopy = questions;
    questionsCopy[questionIndex].correctAnswer = i;
    setQuestions([...questionsCopy]);
  };

  return (
    <div className={classes.container}>
      <div className={classes.top}>
        <h1>{questionIndex + 1}</h1>
        <h5 onClick={() => handleRemoveQuestion(questionIndex)}>&times;</h5>
      </div>
      <div className={classes.bottom}>
        <textarea
          rows="2"
          maxLength="50"
          onChange={handleQuestionChange}
          type="text"
          value={question.question}
        />
        <div className={classes.options}>
          {question.answers.map((option, i) => {
            return (
              <div key={i} className={classes.option}>
                <h6
                  className={correctAnswer === i ? classes.correctAnswer : ''}
                  onClick={() => setCorrectAnswer(i)}
                >
                  {letters[i]}
                </h6>
                <input
                  type="text"
                  onChange={e => handleOptionChange(i, e)}
                  maxLength="50"
                  value={option}
                />
                <h5 onClick={() => removeOption(i)}>&times;</h5>
              </div>
            );
          })}
          <p>Click the letter on the correct answer</p>
          <button onClick={addOption}>+ add option</button>
        </div>
      </div>
    </div>
  );
};

export default React.memo(CreateQuestionCard);
