import React from 'react';
import classes from './back-button.module.scss';
import { useRouter } from 'next/router';

const BackButton = props => {
  const router = useRouter();
  return (
    <div className={classes.container}>
      <p onClick={() => router.back()}>&lsaquo; Back</p>
    </div>
  );
};

export default React.memo(BackButton);
