import React, { useState, useEffect, useRef } from 'react';
import MainHeading from 'components/main-heading';
import PageTitle from 'components/page-title';
import BackButton from 'components/back-button';
import Nav from 'components/nav';
import Login from 'components/login';
import Register from 'components/register';
import classes from './layout.module.scss';

const useClickOutside = (ref, cb) => {
  useEffect(() => {
    const handleClickOutside = e => {
      if (ref.current && !ref.current.contains(e.target)) return cb(false);
    };

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [ref]);
};

const Layout = props => {
  const [showLogin, setShowLogin] = useState(false);
  const [showRegister, setShowRegister] = useState(false);
  const { token, pageTitle } = props;
  const loginBox = useRef(null);
  const registerBox = useRef(null);

  useClickOutside(loginBox, setShowLogin);
  useClickOutside(registerBox, setShowRegister);
  return (
    <header className={classes.container}>
      <BackButton />
      {showLogin && <Login setShowLogin={setShowLogin} loginBox={loginBox} />}
      {showRegister && (
        <Register setShowRegister={setShowRegister} registerBox={registerBox} />
      )}
      <MainHeading />
      <Nav
        token={token}
        setShowLogin={setShowLogin}
        setShowRegister={setShowRegister}
      />
      <PageTitle title={pageTitle} />
      {props.children}
    </header>
  );
};

export default React.memo(Layout);
