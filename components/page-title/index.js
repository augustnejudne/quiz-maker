import React from 'react';
import classes from './page-title.module.scss';

const PageTitle = props => {
  const { title } = props;
  return (
    <div className={classes.titleContainer}>
      <h2>{title}</h2>
    </div>
  );
};

export default React.memo(PageTitle);
