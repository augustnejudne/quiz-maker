import React from 'react';
import classes from 'loading.module.scss';

const Loading = props => {
  return (
    <div className={classes.container}>
      <div className={classes['lds-roller']}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default React.memo(Loading);
