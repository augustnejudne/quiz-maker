import icon1 from 'icon1.svg';
import icon2 from 'icon2.svg';
import icon3 from 'icon3.svg';
import icon4 from 'icon4.svg';
import icon5 from 'icon5.svg';
import icon6 from 'icon6.svg';
import icon7 from 'icon7.svg';
import icon8 from 'icon8.svg';
import icon9 from 'icon9.svg';
import icon10 from 'icon10.svg';
import icon11 from 'icon11.svg';
import icon12 from 'icon12.svg';
import icon13 from 'icon13.svg';
import icon14 from 'icon14.svg';
import icon15 from 'icon15.svg';
import icon16 from 'icon16.svg';
import icon17 from 'icon17.svg';
import icon18 from 'icon18.svg';
import icon19 from 'icon19.svg';
import icon20 from 'icon20.svg';
import icon21 from 'icon21.svg';
import icon22 from 'icon22.svg';
import icon23 from 'icon23.svg';
import icon24 from 'icon24.svg';
import icon25 from 'icon25.svg';
import icon26 from 'icon26.svg';
import icon27 from 'icon27.svg';
import icon28 from 'icon28.svg';
import icon29 from 'icon29.svg';
import icon30 from 'icon30.svg';
import icon31 from 'icon31.svg';
import icon32 from 'icon32.svg';
import icon33 from 'icon33.svg';
import icon34 from 'icon34.svg';
import icon35 from 'icon35.svg';
import icon36 from 'icon36.svg';

export default [
  icon1,
  icon2,
  icon3,
  icon4,
  icon5,
  icon6,
  icon7,
  icon8,
  icon9,
  icon10,
  icon11,
  icon12,
  icon13,
  icon14,
  icon15,
  icon16,
  icon17,
  icon18,
  icon19,
  icon20,
  icon21,
  icon22,
  icon23,
  icon24,
  icon25,
  icon26,
  icon27,
  icon28,
  icon29,
  icon30,
  icon31,
  icon32,
  icon33,
  icon34,
  icon35,
  icon36,
];
