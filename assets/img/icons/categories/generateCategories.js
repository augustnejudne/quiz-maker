const fs = require('fs');

let string = '';
let array = '';

fs.readdir(__dirname, (err, files) => {
  if (err) {
    return console.log('err', err);
  }
  var reA = /[^a-zA-Z]/g;
  var reN = /[^0-9]/g;
  function sortAlphaNum(a, b) {
    var aA = a.replace(reA, '');
    var bA = b.replace(reA, '');
    if (aA === bA) {
      var aN = parseInt(a.replace(reN, ''), 10);
      var bN = parseInt(b.replace(reN, ''), 10);
      return aN === bN ? 0 : aN > bN ? 1 : -1;
    } else {
      return aA > bA ? 1 : -1;
    }
  }

  const filesSorted = files.sort(sortAlphaNum);

  filesSorted.forEach((file, i) => {
    if (file.includes('svg')) {
      string += `\nimport ${file.replace('.svg', '')} from '${file}'`;
      array += `\n${file.replace('.svg', '')},`;
    }
  });

  const writeThis = `
  ${string}

  export default [${array}]
  `;

  fs.writeFile(__dirname + '/categories.js', writeThis, err => {
    if (err) {
      return console.log(err);
    }

    console.log('write success');
  });
});
