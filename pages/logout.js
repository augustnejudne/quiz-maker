import React, { useEffect } from 'react';
import { Consumer } from 'contexts';
import { useRouter } from 'next/router';

const Logout = props => {
  const { logout } = props;
  const router = useRouter();
  useEffect(() => {
    logout() && router.push('/');
  }, []);
  return <div>logging out</div>;
};

export default Consumer(React.memo(Logout), ({ logout }) => ({
  logout,
}));
