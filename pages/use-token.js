import React, { useEffect } from 'react';
import { Consumer } from 'contexts';
import { useRouter } from 'next/router';

const Verify = props => {
  const { setToken } = props;
  const router = useRouter();

  useEffect(() => {
    setToken(router.query.token).then(res => {
      if (res) {
        router.push('/');
      }
    });
  }, [router]);
  return <div />;
};

export default Consumer(React.memo(Verify), ({ setToken }) => ({
  setToken,
}));
