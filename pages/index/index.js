import React, { useState, useEffect } from 'react';
import Layout from 'components/layout';
import QuizCard from 'components/quiz-card';
import classes from './index.module.scss';
import Loading from 'components/loading';
import { Consumer } from 'contexts';

const Index = props => {
  const { quizzes, getQuizzes, searchQuizzes } = props;
  const [loading, setLoading] = useState(false);
  const [searchQuery, set_searchQuery] = useState('');

  useEffect(() => {
    setLoading(true);
    getQuizzes().then(() => setLoading(false));
  }, []);

  const handleChange = e => {
    set_searchQuery(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (!searchQuery) {
      getQuizzes().then(() => setLoading(false));
    }
    setLoading(true);
    searchQuizzes(searchQuery).then(() => setLoading(false));
  };

  return (
    <Layout pageTitle="#explore">
      <form onSubmit={handleSubmit} className={classes.searchContainer}>
        <input
          onChange={handleChange}
          value={searchQuery}
          type="text"
          placeholder="search"
        />
        <button type="submit">search</button>
      </form>
      {loading ? (
        <Loading />
      ) : (
        <>
          <main className={classes.quizzesContainer}>
            {quizzes.map((quiz, i) => {
              return <QuizCard quiz={quiz} key={i} />;
            })}
          </main>
        </>
      )}
    </Layout>
  );
};

export default Consumer(
  React.memo(Index),
  ({ quizzes, getQuizzes, searchQuizzes }) => ({
    quizzes,
    getQuizzes,
    searchQuizzes,
  })
);
