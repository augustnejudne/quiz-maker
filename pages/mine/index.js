import React, { useState, useEffect } from 'react';
import Layout from 'components/layout';
import { Consumer } from 'contexts';
import QuizCard from 'components/quiz-card';
import { isEmpty } from 'underscore';
import classes from './mine.module.scss';
import Loading from 'components/loading';

const Mine = props => {
  const { quizzes, getQuizzes, user, deleteQuiz } = props;
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    if (user) {
      getQuizzes(user._id).then(() => setLoading(false));
    }
  }, [user]);

  if (isEmpty(user)) {
    return <div />;
  }

  return (
    <Layout pageTitle="#mine">
      {loading ? (
        <Loading />
      ) : (
        <main className={classes.quizzesContainer}>
          {quizzes.map((quiz, i) => {
            return (
              <QuizCard key={i} deleteQuiz={deleteQuiz} update quiz={quiz} />
            );
          })}
        </main>
      )}
    </Layout>
  );
};

export default Consumer(
  React.memo(Mine),
  ({ user, quizzes, getQuizzes, deleteQuiz }) => ({
    user,
    quizzes,
    getQuizzes,
    deleteQuiz,
  }),
  true
);
