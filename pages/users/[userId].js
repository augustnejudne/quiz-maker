import React, { useState, useEffect } from 'react';
import Layout from 'components/layout';
import { Consumer } from 'contexts';
import QuizCard from 'components/quiz-card';
import classes from './users.module.scss';
import { useRouter } from 'next/router';
import Loading from 'components/loading';

const Users = props => {
  const router = useRouter();
  const userId = router.query.userId;
  const { quizzes, getQuizzes, visitedUser, getVisitedUser } = props;
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getData = async () => {
      if (userId) {
        setLoading(true);
        await getQuizzes(userId);
        await getVisitedUser(userId);
        setLoading(false);
      }
    };
    getData();
  }, [router]);

  return (
    <Layout pageTitle={!loading && `quizzes by ${visitedUser.username}`}>
      {loading ? (
        <Loading />
      ) : (
        <main className={classes.quizzesContainer}>
          {quizzes.map((quiz, i) => {
            return <QuizCard key={i} quiz={quiz} />;
          })}
        </main>
      )}
    </Layout>
  );
};

export default Consumer(
  React.memo(Users),
  ({ quizzes, getQuizzes, visitedUser, getVisitedUser }) => ({
    quizzes,
    getQuizzes,
    visitedUser,
    getVisitedUser,
  })
);
