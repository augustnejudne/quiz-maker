import React, { Fragment, useState, useEffect } from 'react';
import Layout from 'components/layout';
import classes from './quiz.module.scss';
import { useRouter } from 'next/router';
import { Consumer } from 'contexts';
import QuestionCard from 'components/question-card';
import { isEmpty } from 'underscore';
import Loading from 'components/loading';

const Quizzes = props => {
  const { submitScore, quiz, setCurrentQuiz } = props;
  const router = useRouter();
  const quizId = router.query.quizId;
  const [questionsAnswered, setQuestionsAnswered] = useState(0);
  const [correctAnswers, setCorrectAnswers] = useState(0);
  const quizLoaded = !isEmpty(quiz);
  const questionsLeftToAnswer =
    quizLoaded && quiz.questions.length - questionsAnswered;
  const [submitScoreSuccess, setSubmitScoreSuccess] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (quizId) {
      setCurrentQuiz(quizId).then(res => {
        if (!res) {
          router.push('/404');
        }
        setLoading(false);
      });
    }
  }, [quizId]);

  useEffect(() => {
    setLoading(true);
  }, []);

  useEffect(() => {
    if (questionsLeftToAnswer === 0) {
      submitScore(quizId)
        .then(res => {
          if (!res) {
            return setSubmitScoreSuccess(false);
          }
          return setSubmitScoreSuccess(true);
        })
        .catch(() => {
          return setSubmitScoreSuccess(false);
        });
    }
  }, [questionsLeftToAnswer]);

  return (
    <Layout pageTitle={quiz.title}>
      {loading ? (
        <Loading />
      ) : (
        <Fragment>
          <div className={classes.questionsContainer}>
            <div className={classes.correctAnswers}>
              {questionsAnswered > 0 && (
                <h1>{`${correctAnswers}/${
                  quizLoaded && quiz.questions.length
                }`}</h1>
              )}
            </div>
            {quizLoaded &&
              quiz.questions.map((question, i) => {
                return (
                  <QuestionCard
                    key={i}
                    i={i}
                    correctAnswers={correctAnswers}
                    setCorrectAnswers={setCorrectAnswers}
                    questionsAnswered={questionsAnswered}
                    setQuestionsAnswered={setQuestionsAnswered}
                    question={question}
                  />
                );
              })}
          </div>
          <div className={classes.youGot}>
            {quizLoaded && questionsAnswered !== quiz.questions.length ? (
              <h1>
                You have {questionsLeftToAnswer} more question
                {questionsLeftToAnswer > 1 && 's'} to answer.
              </h1>
            ) : (
              <Fragment>
                <h1>
                  You got {correctAnswers} out of{' '}
                  {quizLoaded && quiz.questions.length} !
                </h1>
                <h5>{submitScoreSuccess && 'Successfully submitted score'}</h5>
                <h5>{submitScoreSuccess === false && 'Failed to submit score'}</h5>
                <div className={classes.takeMore}>
                  <h2 onClick={() => router.push('/')}>Take more quizzes!</h2>
                </div>
              </Fragment>
            )}
          </div>
        </Fragment>
      )}
    </Layout>
  );
};

export default Consumer(
  React.memo(Quizzes),
  ({ quiz, setCurrentQuiz, token, submitScore }) => ({
    submitScore,
    quiz,
    setCurrentQuiz,
    token,
  })
);
