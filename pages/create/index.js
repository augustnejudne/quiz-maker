import React, { useState } from 'react';
import Layout from 'components/layout';
import CreateQuestionCard from 'components/create-question-card';
import categories from 'assets/img/icons/categories/categories';
import QuizCard from 'components/quiz-card';
import classes from './create.module.scss';
import { Consumer } from 'contexts';
import { isEmpty } from 'underscore';
import { useRouter } from 'next/router';

const Create = props => {
  const router = useRouter();
  const { submitQuiz, user } = props;
  const maxChars = 50;
  const [chars, setChars] = useState(maxChars);
  const [title, setTitle] = useState('');
  const [colorScheme, setColorScheme] = useState(0);
  const [icon, setIcon] = useState(0);
  const [errors, setErrors] = useState({});
  const [questions, setQuestions] = useState([
    {
      question: '',
      answers: ['', ''],
      correctAnswer: 0,
    },
  ]);
  const [submitStatus, setSubmitStatus] = useState();

  const autoGrow = e => {
    e.currentTarget.style.height = e.currentTarget.scrollHeight + 'px';
    setChars(maxChars - e.currentTarget.value.length);
  };

  const handleAddQuestion = () => {
    setQuestions([
      ...questions,
      { question: '', answers: ['', ''], correctAnswer: 0 },
    ]);
  };

  const handleRemoveQuestion = index => {
    if (questions.length === 1) return;
    const questionsCopy = questions;
    questions.splice(index, 1);
    setQuestions([...questionsCopy]);
  };

  const handleTitleChange = e => {
    autoGrow(e);
    setTitle(e.currentTarget.value);
  };

  const handleSubmitQuiz = () => {
    if (submitStatus === 'success') {
      return router.push('/mine');
    }

    const quizBody = {
      creator: user._id,
      title,
      colorScheme,
      icon,
      questions,
    };

    quizBody.questions = quizBody.questions.filter(q => q.question !== '');
    // remove blank answers and assign them to answersAnswersFiltered
    quizBody.questions.forEach(q => {
      q.answersFiltered = q.answers.filter(a => a !== '');
    });

    if (
      !title ||
      quizBody.questions.length === 0 ||
      (quizBody.questions[0] &&
        quizBody.questions[0].answersFiltered.length < 2)
    ) {
      const errorsCopy = errors;
      if (!title) {
        errorsCopy.title = 'Title is required';
      }

      if (quizBody.questions.length === 0) {
        errorsCopy.questions = 'You need at least one question';
      }

      if (
        quizBody.questions[0] &&
        quizBody.questions[0].answersFiltered.length < 2
      ) {
        errorsCopy.answers = 'Your questions need at least 2 options';
      }

      setErrors({ ...errorsCopy });
      return;
    }

    setErrors({});

    setSubmitStatus('loading');

    // before submitting, assign answersFiltered to answers
    quizBody.questions.forEach(q => {
      q.answers = q.answersFiltered;
    });

    submitQuiz(quizBody)
      .then(res => {
        if (!res) return setSubmitStatus('fail');
        return setSubmitStatus('success');
      })
      .catch(() => {
        setSubmitStatus('fail');
      });
  };

  const renderErrors = () => {
    return Object.values(errors).map((e, i) => {
      return <p key={i}>{e}</p>;
    });
  };

  if (isEmpty(user)) {
    return <div />;
  }

  return (
    <Layout pageTitle="#create">
      <main className={classes.createContainer}>
        <div className={classes.titleContainer}>
          <textarea
            autoFocus
            rows="1"
            maxLength="50"
            type="text"
            value={title}
            onChange={handleTitleChange}
          />
          <h6>title</h6>
          <p>{chars} characters left</p>
        </div>

        {questions.map((question, i) => {
          return (
            <CreateQuestionCard
              key={i}
              questions={questions}
              questionIndex={i}
              question={question}
              handleRemoveQuestion={handleRemoveQuestion}
              setQuestions={setQuestions}
            />
          );
        })}
        <button onClick={handleAddQuestion}>+ add question</button>
        <h5>Pick a color scheme</h5>
        <div className={classes.colorSchemesContainer}>
          {[0, 1, 2, 3, 4, 5, 6].map((c, i) => {
            return (
              <div
                key={i}
                onClick={() => setColorScheme(i)}
                style={{
                  border: colorScheme === i ? '2px solid #000' : '',
                }}
              />
            );
          })}
        </div>
        <h5>Pick an icon</h5>
        <div className={classes.categoriesContainer}>
          {categories.map((c, i) => {
            return (
              <img
                key={i}
                src={c}
                alt=""
                onClick={() => setIcon(i)}
                style={{ border: icon === i ? '2px solid #000' : '' }}
              />
            );
          })}
        </div>
        <h6>Your quiz card will look like this</h6>
        <div className={classes.quizCardContainer}>
          <QuizCard
            displayOnly
            quiz={{
              creator: {
                username: user.username,
              },
              title,
              questions,
              createdAt: new Date(),
              takers: 0,
              colorScheme,
              icon,
            }}
          />
        </div>
        {!isEmpty(errors) && (
          <div className={classes.errorsContainer}>{renderErrors()}</div>
        )}
        {submitStatus === 'success' && (
          <h6 className={classes.successText}>Successfully submitted quiz</h6>
        )}
        {submitStatus === 'fail' && (
          <h6 className={classes.failText}>Failed to submit quiz</h6>
        )}
        <button
          disabled={setSubmitStatus === 'loading'}
          className={
            submitStatus === 'success'
              ? classes.goToMyQuizzes
              : classes.createQuizButton
          }
          onClick={handleSubmitQuiz}
        >
          {submitStatus === 'success' ? 'Go to my quizzes' : 'Create Quiz'}
        </button>
      </main>
    </Layout>
  );
};

export default Consumer(
  React.memo(Create),
  ({ submitQuiz, user }) => ({
    submitQuiz,
    user,
  }),
  true
);
