import React from 'react';
import App from 'next/app';
import 'styles/index.scss';
import { Provider } from 'contexts';
import Head from 'next/head';

class QuizMakerApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <>
        <Head>
          <title>Quiz Maker</title>
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width"
          />
        </Head>
        <Provider>
          <Component {...pageProps} />
        </Provider>
      </>
    );
  }
}

export default QuizMakerApp;
