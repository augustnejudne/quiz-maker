import React, { createContext, useState, useEffect } from 'react';
import { findQuiz } from 'utilities/search-local';
import axios from 'axios';
import { useRouter } from 'next/router';

const api = axios.create({
  baseURL: 'https://frozen-falls-15816.herokuapp.com/',
  // baseURL: 'http://localhost:5000',
  headers: {
    'Content-Type': 'application/json',
  },
});

const Context = createContext();

export const Consumer = (Component, states, withAuth) => {
  const WrapperComponent = props => {
    const router = useRouter();
    useEffect(() => {
      const token = localStorage.getItem('quiz-maker-token');
      if (withAuth && token === null) {
        router.push('/');
      }
    }, []);
    return (
      <Context.Consumer>
        {state => {
          return <Component {...props} {...states(state)} />;
        }}
      </Context.Consumer>
    );
  };

  return WrapperComponent;
};

export const Provider = props => {
  const [quizzes, setQuizzes] = useState([]);
  const [quiz, setQuiz] = useState({});
  const [user, setUser] = useState();
  const [visitedUser, setVisitedUser] = useState({});

  useEffect(() => {
    const token = localStorage.getItem('quiz-maker-token');
    if (token) {
      setToken(token);
    } else {
      delete api.defaults.headers.common['Authorization'];
    }
  }, []);

  const getVisitedUser = async userId => {
    try {
      const res = await api.get(`/users/profiles/${userId}`);
      if (res.status !== 200) {
        return false;
      }
      setVisitedUser(res.data);
    } catch (e) {
      return false;
    }
  };

  const loginUser = async details => {
    try {
      const res = await api.post(`/auth/login`, details);
      if (res.status !== 200) return false;
      return res.data;
    } catch (e) {
      return false;
    }
  };

  const registerUser = async details => {
    try {
      const res = await api.post(`/auth/register`, details);
      if (res.status !== 200) return false;
      return res.data;
    } catch (e) {
      return false;
    }
  };

  const getQuizzes = async creatorId => {
    try {
      if (!creatorId) {
        const res = await api.get('/quizzes');
        if (res.status !== 200) return false;
        setQuizzes(res.data);
        return true;
      }
      const res = await api.get(`/quizzes/?creator=${creatorId}`);
      if (res.status !== 200) return false;
      setQuizzes(res.data);
      return true;
    } catch (e) {
      return false;
    }
  };

  const submitScore = async quizId => {
    try {
      const res = await api.put(`/quizzes/${quizId}?submit=true`);
      if (res.status !== 200) return false;
      return true;
    } catch (e) {
      return false;
    }
  };

  const setCurrentQuiz = async quizId => {
    const fQuiz = findQuiz(quizId);
    if (!fQuiz) {
      try {
        const res = await api.get(`/quizzes/${quizId}`);
        await setQuiz(res.data);
        return true;
      } catch (e) {
        return false;
      }
    }
    setQuiz(fQuiz);
    return true;
  };

  const setToken = async token => {
    api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    try {
      if (!token) return false;
      localStorage.setItem('quiz-maker-token', token);
      const res = await api.get(`/users/${token}`);
      if (res.status !== 200) return false;
      setUser(res.data);
      return true;
    } catch (e) {
      return false;
    }
  };

  const logout = async () => {
    const token = localStorage.getItem('quiz-maker-token');
    const res = await api.get(`/auth/logout/${token}`);
    if (res.status !== 200) {
      return false;
    }

    localStorage.removeItem('quiz-maker-token');
    setUser();
    return true;
  };

  const submitQuiz = async quiz => {
    try {
      const res = await api.post('/quizzes', quiz);
      if (res.status !== 200) return false;
      return true;
    } catch (e) {
      return false;
    }
  };

  const editQuiz = async quizBody => {
    try {
      const res = await api.put(`/quizzes/${quiz._id}`, quizBody);
      if (res.status !== 200) return false;
      return true;
    } catch (e) {
      return false;
    }
  };

  const deleteQuiz = async quizId => {
    try {
      const res = await api.delete(`/quizzes/${quizId}`);
      if (res.status !== 200) return false;
      setQuizzes([...quizzes.filter(q => q._id !== quizId)]);
      return true;
    } catch (e) {
      return false;
    }
  };

  const searchQuizzes = async query => {
    try {
      const res = await api.get(`/quizzes/?search=${query}`);
      if (res.status !== 200) return false;
      setQuizzes(res.data);
      return true;
    } catch (e) {
      return false;
    }
  };

  const value = {
    deleteQuiz,
    setToken,
    submitScore,
    logout,
    user,
    quiz,
    setCurrentQuiz,
    submitQuiz,
    editQuiz,
    quizzes,
    getQuizzes,
    visitedUser,
    getVisitedUser,
    loginUser,
    registerUser,
    searchQuizzes,
  };
  return <Context.Provider value={value}>{props.children}</Context.Provider>;
};
