import quizzes from 'ignore/quizzes';

// find quiz
export const findQuiz = quizId => {
  return quizzes.find(q => q._id === quizId) || false;
};
